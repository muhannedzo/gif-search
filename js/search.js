$( document ).ready(function() {

    function renderJson(id, object) {

        var input = eval('(' + JSON.stringify(object) + ')');
        var options = {
            collapsed: true,
            rootCollapsable: true,
            withQuotes: false,
            withLinks: true
        };
        $('#' + id).jsonViewer(input, options);
    }



    $('#search-btn').on('click', function (e) {
        e.preventDefault();

        var searchText = $('#Searchbox').val();

        var gifApi = "https://api.giphy.com/v1/gifs/search?";
        var apiKey = "93y1OoAQVK08RTPk2TmOqGfSnuosm8vw";
        var limit = 12;
        var offset = 0;
        var rating = 'G';
        var lang = 'en';

        var url = gifApi + 'api_key=' + apiKey + '&q=' + searchText + '&limit=' + limit + '&offset=' + offset + '&eating=' + rating + '&lang=' + lang;

        jQuery.get(url)
            .done(function (response) {
                $('#sentUrl').text(url);
                renderJson('json-renderer', response);
                if(response.meta.status == 200){

                    $('#gifdiv').empty();
                    for(var i = 0; i < response.data.length; i++){
                        $('#gifdiv').append('\
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 card-div">\
                                    <div class="card" style="width: 18rem;">\
                                        <img src="' + response.data[i].images.fixed_height_downsampled.url + '" class="card-img-top gif" alt="' + response.data[i].title + '"/>\
                                        <div class="card-body">\
                                            <h5 class="card-title">' + response.data[i].title + '</h5>\
                                        </div>\
                                    </div>\
                                </div>\
                                ');
                    }
                }else{
                    notification('danger', 'something went wrong');
                }

            });
    });
});











