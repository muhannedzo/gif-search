$(document).ready(function(){
    $('#signup-form').submit(function(e){
        e.preventDefault();
        var username = $('#signup-username').val();
        var password = $('#signup-password').val();

        var parameters = {
            user_name: username,
            password: password
        };

        $.post("requests/signup.php", parameters).done(function(response){
            window.location.href = "./login.php";
        });
    });
});