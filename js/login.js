$(document).ready(function(){
    $('#login-form').submit(function(e){
        e.preventDefault();
        var username = $('#login-username').val();
        var password = $('#login-password').val();

        var parameters = {
            user_name: username,
            password: password
        };

        $.post("requests/login.php", parameters).done(function(response){
            window.location.href = "./index.php";
        });
    });
});