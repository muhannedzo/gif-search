<?php
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Home Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="json-viewer/jquery.json-viewer.js"></script>
    <link href="json-viewer/jquery.json-viewer.css" type="text/css" rel="stylesheet" />

</head>
<body>
    <div class="row">
        <div class="col-9 container-div-all">
            <div class="container container-div">
                <div class="container container-inside-div">
                    <div class="row">
                        <p class="title-div">Welcome, feel free to search for any Gif you want</p>
                    </div>
                    <hr />
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <div class="kt-input-icon kt-input-icon--left">
                                    <input type="text" class="form-control" placeholder="Search" id="Searchbox">
                                </div>
                            </div>
                            <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                <button type="button" class="btn btn-primary" id="search-btn"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-6 kt-margin-b-20-tablet-and-mobile">
                                <pre id="json-renderer1"></pre>
                            </div>
                            <div class="col-md-6 kt-margin-b-20-tablet-and-mobile">
                                <pre id="json-renderer2"></pre>
                            </div>
                            <div class="col-md-12 kt-margin-b-20-tablet-and-mobile">
                                <p id="sentUrl"></p>
                                <pre id="json-renderer"></pre>
                            </div>
                        </div> -->
                        <div id="gifdiv" class="row gifdiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="js/search.js"></script>
<!-- <script type="text/javascript" src="js/logout.js"></script> -->

</body>
</html>